# Compila os drivers do Oracle em um container temporário
FROM registry.access.redhat.com/rhel7.6 as db_driver
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ARG REDHAT_USERNAME
ARG REDHAT_PASSWORD

# Registra e habilita o canal do Software Collection
RUN subscription-manager register --auto-attach --username ${REDHAT_USERNAME} --password ${REDHAT_PASSWORD} --force \
# Habilita o SCL para as ultimas versao do PHP
&& subscription-manager repos --enable=rhel-server-rhscl-7-rpms --enable=rhel-7-server-optional-rpms \
# Instala os arquivos necessarios para compilar os drivers
&& yum install -y --nogpgcheck rh-php72-php-pear rh-php72-php-devel libaio unixODBC-devel gcc-c++ gcc bc make zlib-devel libmemcached-devel \
&& source scl_source enable rh-php72 \
# Instala o módulo para o redis
&& pecl install redis-5.0.2 \
# Instala o módulo para o memcached
&& pecl install memcached-3.1.3 \
# Instala os drivers do SQL Server
&& pecl install sqlsrv-5.6.0 \
&& pecl install pdo_sqlsrv-5.6.0 \
# Instala os drivers do Oracle
&& curl -o /tmp/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm https://s3-sa-east-1.amazonaws.com/davikondo/oracle/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm \
&& curl -o /tmp/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm https://s3-sa-east-1.amazonaws.com/davikondo/oracle/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm \
&& rpm -iv /tmp/*.rpm \
&& echo '' | pecl install oci8-2.2.0 \
&& subscription-manager remove --all \
&& subscription-manager unregister \
&& subscription-manager clean

# Instalacao oficial do RHEL7
FROM registry.access.redhat.com/rhel7.6
# Configura o locale para UTF-8
ENV LANG=en_US.UTF-8 
ENV LC_ALL=en_US.UTF-8
ARG REDHAT_USERNAME
ARG REDHAT_PASSWORD

# Registra e habilita o canal do Software Collection
RUN subscription-manager register --auto-attach --username ${REDHAT_USERNAME} --password ${REDHAT_PASSWORD} --force \
# Habilita o SCL para as ultimas versoes do Nginx, NodeJS e PHP
&& subscription-manager repos --enable=rhel-server-rhscl-7-rpms \
# Adiciona repositório da Microsoft para instalação do ODBC próprio - Adicionar o ACCEPT_EULA=Y ao instalar
&& curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/mssql-release.repo \
# Instala PHP-FPM e as dependencias necessarias
&& ACCEPT_EULA=Y yum install -y --nogpgcheck rh-php72-php-opcache rh-php72-php-mysqlnd rh-php72-php-pgsql rh-php72-php-pdo rh-php72-php-ldap rh-php72-php rh-php72-php-mbstring rh-php72-php-cli rh-php72-php-xml rh-php72-php-json libmemcached \
# Instala Apache
httpd24 \
# Instala dependencia para os drivers da Oracle
libaio \
# Instala dependência para o driver do SQL Server
msodbcsql17 \
# Instala o utilitário de PDF
https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox-0.12.5-1.centos7.x86_64.rpm
# Copia o arquivo compilado para a imagem atual
COPY --from=db_driver /opt/rh/rh-php72/root/usr/lib64/php/modules/oci8.so /opt/rh/rh-php72/root/usr/lib64/php/modules
COPY --from=db_driver /opt/rh/rh-php72/root/usr/lib64/php/modules/pdo_sqlsrv.so /opt/rh/rh-php72/root/usr/lib64/php/modules
COPY --from=db_driver /opt/rh/rh-php72/root/usr/lib64/php/modules/sqlsrv.so /opt/rh/rh-php72/root/usr/lib64/php/modules
COPY --from=db_driver /opt/rh/rh-php72/root/usr/lib64/php/modules/memcached.so /opt/rh/rh-php72/root/usr/lib64/php/modules
COPY --from=db_driver /opt/rh/rh-php72/root/usr/lib64/php/modules/redis.so /opt/rh/rh-php72/root/usr/lib64/php/modules
# Copia os e instala os drivers necessarios para utilizacao do OCI
COPY --from=db_driver /tmp /tmp
RUN rpm -iv /tmp/*.rpm \
# Configura timezone
&& cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
# Habilita modulos no PHP
&& echo "extension=oci8.so" > /etc/opt/rh/rh-php72/php.d/20-oci.ini \
&& echo "extension=pdo_sqlsrv.so" > /etc/opt/rh/rh-php72/php.d/30-pdo_sqlsrv.ini \
&& echo "extension=sqlsrv.so" > /etc/opt/rh/rh-php72/php.d/20-sqlsrv.ini \
&& echo "extension=memcached.so" > /etc/opt/rh/rh-php72/php.d/10-memcached.ini \
&& echo "extension=redis.so" > /etc/opt/rh/rh-php72/php.d/90-redis.ini \
&& printf '#!/bin/bash\nsource scl_source enable rh-php72' > /etc/profile.d/rh-php72.sh \
&& printf '#!/bin/bash\nsource scl_source enable httpd24' > /etc/profile.d/httpd24.sh \
&& printf '#!/bin/bash\nexport PATH=/opt/rh/httpd24/root/usr/bin:/opt/rh/httpd24/root/usr/sbin${PATH:+:${PATH}}\nexport MANPATH=/opt/rh/httpd24/root/usr/share/man:${MANPATH}\nexport PKG_CONFIG_PATH=/opt/rh/httpd24/root/usr/lib64/pkgconfig${PKG_CONFIG_PATH:+:${PKG_CONFIG_PATH}}\nexport LIBRARY_PATH=/opt/rh/httpd24/root/usr/lib64${LIBRARY_PATH:+:${LIBRARY_PATH}}\nexport LD_LIBRARY_PATH=/opt/rh/httpd24/root/usr/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}\n/opt/rh/httpd24/root/usr/sbin/httpd -D FOREGROUND' > entrypoint.sh \
&& chmod +x /etc/profile.d/rh-php72.sh /etc/profile.d/httpd24.sh /entrypoint.sh \
#&& printf '#!/bin/bash\nsource scl_source enable rh-nginx114' > /etc/profile.d/rh-nginx114.sh \
# Ajustes na configuração do PHP-FPM
#&& sed -i 's|daemonize = yes|daemonize = no|g' /etc/opt/rh/rh-php72/php-fpm.conf \
#&& sed -i 's|127.0.0.1|0.0.0.0|g' /etc/opt/rh/rh-php72/php-fpm.d/www.conf \
# Apaga os arquivos no TMP
&& rm -f /tmp/*.rpm \
&& subscription-manager remove --all \
&& subscription-manager unregister \
&& subscription-manager clean

EXPOSE 80/tcp

CMD [ "bash","/entrypoint.sh" ]